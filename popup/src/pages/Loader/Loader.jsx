//styles
import classes from './Loader.module.css'

const Loader = () =>{
    return(
        <div className={classes.container}>
            <div className={classes.line}/>
            <div className={classes.line}/>
            <div className={classes.line}/>
        </div>
    )
}

export default Loader;
