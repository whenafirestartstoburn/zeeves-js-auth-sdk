//styles
import classes from './ImportPage.module.css'

//npm package
import {Link} from "react-router-dom";

const ImportPage = () => {
    return(
        <div className={classes.container}>
            <div>
                <p>
                    You can generate new Zilliqa Wallet QR Code using existing private keys or mnemonic
                </p>
                <Link className={classes.link} to={'/en/import-no-create/manual'}>Use private keys </Link>
                <Link className={classes.link} to={'/en/import-no-create/mnemonic'}>Use mnemonic phrase</Link>
            </div>
            <div>
                <p>
                    If you already have Zilliqa Wallet QR code you can import it
                </p>
                <Link className={classes.link} to={'/en/import-no-create/qr'}>Import Zilliqa Wallet QR code</Link>
            </div>
        </div>
    )
}

export default ImportPage;
