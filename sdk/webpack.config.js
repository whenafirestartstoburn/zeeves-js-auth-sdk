const path = require('path');
const ReplacePlugin = require('webpack-plugin-replace');
const CopyPlugin = require("copy-webpack-plugin");
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');

const config = {
  entry: './sdk.min.js',
  output: {
    path: path.resolve(__dirname, 'sdk/sdk'),
    filename: 'sdk.min.js',
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "./allowedOrigins.json", to: path.resolve(__dirname, 'sdk/sdk') },
        { from: "./package.json", to: path.resolve(__dirname, 'sdk/publish') },
        { from: "./README.MD", to: path.resolve(__dirname, 'sdk/publish') },
      ],
    }),
    new NodePolyfillPlugin(),
  ],
  resolve: {
    fallback: {
        "fs": false,
    },
  },
};

module.exports = (env, argv) => {
  config.mode = argv.mode;
  if (config.mode === 'development') {
    config.devtool = 'source-map';
  }
  config.plugins.push(new ReplacePlugin({
    exclude: [
      /node_modules/,
      'publish.js',
    ],
    values: {
      '<%=selfUrl=>': env.SELF_URL,
      '<%=zilliqaUrl=>': env.ZILLIQA_URL,
      '<%=zeevesSinkUrl=>': env.BACKEND_URL,
    }
  }));
  return config;
};
