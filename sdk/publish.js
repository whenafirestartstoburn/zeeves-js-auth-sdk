const fs = require('fs');


fs.readFile("sdk.min.js", 'utf-8', function(err1, data) {
  if (err1) { 
    console.log(err1);
  }
  
  console.log("Read sdk.min.js content.");

  data = data.replace('<%=selfUrl=>', process.env.SELF_URL);
  data = data.replace('<%=zilliqaUrl=>', process.env.ZILLIQA_URL);
  data = data.replace('<%=zeevesSinkUrl=>', process.env.ZILLIQA_URL);

  console.log("Replacements done.");

  const sdkPath = __dirname + "/sdk/publish/sdk.min.js";

  console.log("Will try to write sdk.min.js to publish directory. Path - " + sdkPath);

  fs.writeFile(sdkPath, data, (err2) => {
    if (err2) {
      console.log(err2);
    }
    console.log("Successfully written target publish sdk.min.js");
  });
});