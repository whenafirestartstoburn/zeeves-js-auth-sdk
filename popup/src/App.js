//styles
import './App.css';

//npm package
import {BrowserRouter, Route, Routes} from "react-router-dom";

//import components
import MainPage from "./pages/MainPage/MainPage";
import ImportPage from "./pages/ImportPage/ImportPage";


function App() {
    return (
        <BrowserRouter>
             <div className="App">
                 <Routes>
                     <Route path='/en/auth-sdk/' element = {<MainPage/>}/>
                     <Route path='/en/import-no-create/' element = {<ImportPage/>}/>
                 </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
