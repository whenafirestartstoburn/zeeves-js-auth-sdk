import localforage from "localforage";

//enum for switch state
export const stateEnum = {
    nickName: 'nickName',
    import: 'import',
    code: 'code'
}

//get private keys from localstorage/localforage
export const getPrivateKeysFromLocalStorage = async () => {
    var CryptoJS = require("crypto-js");
    try {
        //old compatibility
        const pkFromLocalStorage = localStorage.getItem("privateKeys");
        if (pkFromLocalStorage && localforage) {
            localStorage.removeItem("privateKeys");
            await localforage.setItem("privateKeys",  CryptoJS.AES.encrypt(pkFromLocalStorage, process.env.REACT_APP_ZILLIQA_SEED).toString());
        }

        let privateKeys = await localforage.getItem("privateKeys");

        if (!privateKeys) {
            return null;
        }
        privateKeys  = CryptoJS.AES.decrypt(privateKeys, process.env.REACT_APP_ZILLIQA_SEED).toString(CryptoJS.enc.Utf8);
        return !!privateKeys ? JSON.parse(privateKeys).Zilliqa : null;
    } catch(err) {
        console.error('Error getting private keys');
        console.error(err.stack);
        return null;
    }
}

//get message from post window
export const listenMessages = (callback) => {
    const eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    const eventer = window[eventMethod];
    const messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";

    const detach = (e, handler) => {
        if (window.removeEventListener) {
            window.removeEventListener(e, handler, false);
        } else if (window.detachEvent) {
            window.detachEvent(e, handler);
        } else {
            delete window[e];
        }
    }
    const handler = (e) => {
        try {
            callback(e);
        } finally {
            detach(e, handler);
        }
    };

    eventer(messageEvent, handler, false);
    setTimeout(() => {
        detach(messageEvent, handler);
    }, 60*1000);
}



const openCenteredWindow = (url, title, w, h) => {
    const dualScreenLeft = window.screenLeft ? window.screenLeft : window.screenX;
    const dualScreenTop = window.screenTop ? window.screenTop  : window.screenY;

    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : window.screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : window.screen.height;

    const systemZoom = width / window.screen.availWidth;
    const left = (width - w) / 2 / systemZoom + dualScreenLeft
    const top = (height - h) / 2 / systemZoom + dualScreenTop
    const newWindow = window.open(url, title, `scrollbars=yes,width=${w/systemZoom}, height=${h / systemZoom},top=${top},left=${left}`)

    if (window.focus) {
        newWindow.focus();
    }

    return newWindow;
}

//open window for import account
export const importAccount = async (setState) =>{
    await (() => new Promise((resolve, reject) => {
        const importFrame = openCenteredWindow('/en/import-no-create', 'importFrame', 800, 800);
        const interval = setInterval(() => {
            if (importFrame.closed) {
                clearInterval(interval);
                resolve();
            }
        });
    }))();

    await processAuth(setState);
}

//check private keys
export const processAuth = async (setState) =>{
    const privateKeys = await getPrivateKeysFromLocalStorage();
    if(privateKeys){
        setState(stateEnum.code)
    }
    else{
        setState(stateEnum.import)
    }
}
