//styles
import classes from './MainPage.module.css'

//default dependencies
import {useEffect, useState} from "react";

//scripts
import {
    getPrivateKeysFromLocalStorage,
    importAccount,
    listenMessages,
    processAuth, stateEnum
} from "./scripts/scriptsMainPage";

//api
import {postData} from "../../api/api";

//npm package
import localforage from "localforage";
import CryptoJS from "crypto-js";

//import components
import Loader from "../Loader/Loader";


const MainPage = (props) =>{
    const queryParams = new URLSearchParams(window.location.search)
    const [nickName, setNickName] = useState('')
    const [code, setCode] = useState('')
    const [pkPassword, setPkPassword] = useState('')
    const [errorMessageForm, setErrorMessageForm] = useState(<></>)
    const [loader, setLoader] = useState(true)

    //enum events
    const events = {
        ENCRYPTION_KEY: 'encryptionKey',
        AUTH: 'auth',
        ERROR: 'error',
        OPENED: 'opened',
    };

    //state page
    const [state, setState] = useState(stateEnum.nickName)

    //get params from url
    const getPartnerUri = () => {
        return queryParams.get('protocol') + '//' + queryParams.get('partnerUri')
    }

    // get password from the calling website
    const getPkPassword = async () =>{
        try{
            const allowedOrigins = await (await fetch(process.env.REACT_APP_SITE_URL + '/sdk/allowedOrigins.json')).json();
            const messagePayload = events.OPENED;
            const partnerHost = getPartnerUri()
            window.opener.postMessage(btoa(messagePayload), partnerHost);
            const temp_pkPassword = await (() => {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                        reject('Did not receive password from caller origin');
                        console.log(55)
                    }, 30*1000);
                    try {
                        listenMessages((e) => {
                            if (allowedOrigins.findIndex(o => e.origin.startsWith(o)) !== -1) {
                                const key = e.message ? "message" : "data";
                                const data = e[key];
                                if (typeof(data) === 'string') {
                                    if (data.split('_')[0] === events.ENCRYPTION_KEY) {
                                        resolve(data.split('_')[1]);
                                    }
                                }
                            } else {
                                reject('Unknown origin of event - ' + e.origin);
                            }
                        });
                    } catch (err) {
                        console.error('Error receiving password - ' + err.stack);
                        reject('Error receiving password');
                    }
                })
            })();

            if (!temp_pkPassword) {
                alert('Did not receive password from caller origin');
                window.close();
            }


            setPkPassword(temp_pkPassword)

            const lsNickName = localStorage.getItem('nickname');
            const keyExists = !!(await getPrivateKeysFromLocalStorage());
            if (lsNickName && keyExists) {
                setNickName(lsNickName)
                await startAuthentication();
            } else if (!keyExists) {
                localStorage.removeItem('nickname');
            }
        }  catch (err) {
            console.log(pkPassword)
            if(pkPassword !== ''){
                alert('Unexpected error during authentication - ' + err);
                const messagePayload = events.ERROR + "_" + err.message;
                const partnerHost = getPartnerUri();
                window.opener.postMessage(btoa(messagePayload), partnerHost);
            }
        } finally {
            setLoader(false)
        }
    }


    // start authentication with nickname
    const startAuthentication = async () => {
        setLoader(true)
        try {

            if(!nickName && !localStorage.getItem('nickname')){
                return;
            }

            if(localStorage.getItem('nickname')){
                setNickName(localStorage.getItem('nickname'))
            }else if(!nickName){
                return;
            }

            let tempNickname = localStorage.getItem('nickname') ? localStorage.getItem('nickname') : nickName;
            const result = await postData(process.env.REACT_APP_SITE_URL + '/api/auth/start-auth', {
                nickName: tempNickname ? tempNickname.trim() : tempNickname,
                partnerUri: getPartnerUri()
            });
            if (result.success && result.guid) {
                sessionStorage.setItem('authGuid', result.guid);
                localStorage.setItem('nickname', tempNickname)
                setErrorMessageForm(<></>)
                await processAuth(setState);
            } else {
                setErrorMessageForm(
                    <>

                        Error finding Zeeves account and starting authentication. <br/><br/>
                        Are you registered on Zeeves? If not, please register before authentication <br/>
                        <a href={'https://t.me/zilliqawalletbot'} target={'_blank'}>https://t.me/zilliqawalletbot</a>
                    </>
                )
            }
        } catch (err) {
            alert('Unknown error on starting auth');
            const messagePayload = events.ERROR + "_" + err.message;
            const partnerHost = getPartnerUri()
            window.opener.postMessage(btoa(messagePayload), partnerHost);
        } finally {
            setLoader(false)
        }
    }

    //send and check authCode
    const completeAuthentication = async () => {
        setLoader(true)
        try {
            let result;

            try {
                result = await postData(process.env.REACT_APP_SITE_URL + '/api/auth/complete-auth', {
                    guid: sessionStorage.getItem('authGuid'),
                    authCode: code
                });
            } catch (err) {
                alert('error complete auth')
                return;
            }

            if (result.success && result.data) {
                const partnerHost = getPartnerUri();
                const privateKey = await getPrivateKeysFromLocalStorage();
                const encryptedPk = CryptoJS.AES.encrypt(privateKey, pkPassword);
                const messagePayload = events.AUTH + "_" + encryptedPk;
                window.opener.postMessage(btoa(messagePayload), partnerHost);
                window.close();
            } else {
                setErrorMessageForm(
                    <>
                        Invalid authentication code. <br/>
                        Please, try again.
                    </>
                )
                return;
            }
        } catch (err) {
            console.error(err ? err.stack : 'Unknown error on complete auth');

        } finally {
            setLoader(false)
        }
    }

    useEffect( ()=>{
        if (localforage) {
            localforage.config({
                driver: [localforage.INDEXEDDB,
                    localforage.WEBSQL,
                    localforage.LOCALSTORAGE],
                name: process.env.REACT_APP_DB_NAME
            });
        }

        getPkPassword()
    },[])

    return(
        <div className={classes.container}>
          <div className={classes.content}>
              <h3>Zeeves authentication</h3>
              {loader && <Loader/>}
              {state === stateEnum.nickName && !loader &&
                  <div className={classes.formTelegramNickname}>
                      <h4>Please, enter your Telegram nickname, from which you've registered Zeeves account</h4>
                      <input type="text" placeholder={'Enter your telegram nickname'} value={nickName}
                             onKeyPress={(event)=>{
                                 if(event.which == 13 || event.keyCode == 13){
                                     startAuthentication()
                                 }
                             }}
                             onChange={e => setNickName(e.target.value)}/>
                      {errorMessageForm && <span>{errorMessageForm}</span> }
                      <button onClick={startAuthentication}>Continue</button>
                  </div>
              }
              {state === stateEnum.import && !loader &&
                  <div className={classes.formImport}>
                      <h4>You did not import your Zeeves account. Please, import it.</h4>
                      <button onClick={() => importAccount(setState)} >Import</button>
                  </div>
              }
              {state ===  stateEnum.code  && !loader &&
                  <div className={classes.formTelegramCode}>
                      <h4>We've sent you a 6-digit code to your Zeeves account.
                      Please, enter this code below.</h4>
                      <input type={'number'} placeholder={'6-digit code'} value={code} onKeyPress={(event)=>{
                          if(code.length === 6 && (event.which == 13 || event.keyCode == 13)){
                              completeAuthentication()
                          }
                      }}
                             onChange={e => {
                                 console.log(e.target.value)
                                 if(e.target.value.length <= 6)
                                     setCode(e.target.value)
                             }}/>
                      {errorMessageForm && <span>{errorMessageForm}</span> }
                      <button onClick={completeAuthentication}>Continue</button>
                  </div>
              }
          </div>
        </div>
    )
}

export default MainPage;
