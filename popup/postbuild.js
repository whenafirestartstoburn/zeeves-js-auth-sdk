const fs = require('fs')

const replaceFile = (filepath, replacement) => {
  fs.readFile(filepath, 'utf8', (err, data) => {
    if (err) {
      throw new Error('Error reading ' + filepath + ' file! Details - ' + err);
    }
    
    console.log('Will replace /static/ with ' + replacement + ' in ' + filepath + ' to support relative locations');
    
    const result = data.replaceAll('/static/', replacement);
    
    fs.writeFile(filepath, result, 'utf8', function (err) {
      if (err) {
          throw new Error('Error writing new ' + filepath + ' content! Details - ' + err)
      } else {
          console.log('Successfully replaced!');
      }
    });
  });
}

replaceFile('./build/index.html', 'static/');

fs.readdir('./build/static/css', (err, filenames) => {
  filenames.forEach(f => {
    replaceFile('./build/static/css/' + f, '../');
  })
})

